# "Java GUI & Socket Programmierung - Simple Chat"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

### Konzept
Zu aller erst wird immer der Server gestartet, dieser startet einen Thread der dazu beauftragt ist neue Client-Verbindungen anzunehmen und für jede neue Verbindung einen Thread (`ClientWorker`) zu erstellen, der die die Kommunikation zum Client auf Server-Seite vertritt. Als Kommunikations-Mittel werden `PrintWriter` zum Senden und `BufferedReader` zum Empfangen verwendet. Wenn jetzt ein Client gestartet wird, wird eine Verbindung hergestellt und der Client teilt dabei dem Server mit, welchen Chatnamen er verwenden möchte. Der Server evaluiert ob der entsprechende Name gültig ist. Falls ja, sendet er dem Client den Chatnamen unverändert zurück, wenn nicht bekommt der Client einen leicht alternierten Chatnamen zugeschrieben und zugesendet. Der erhaltene Chatname wird dem Client im Chatverlauf angezeigt. Ab jetzt kann der Client an den Server Nachrichten schicken und von diesem auch Empfangen. Natürlich kann sich nicht nur ein Client, sondern beliebig viele mit dem Server verbinden. Wenn jetzt ein Client eine Nachricht sendet, wird diese an alle Clients verteilt. Der Server-Admin kann per grafischer Oberfläche die einzelnen Clients anwählen und per anklicken des Remove-Buttons die Kommunikation zu ihnen beenden. Im Hintergrund wird der dazugehörige `ClientWorker`-Thread sauber geschlossen und der Client bekommt eine Meldung, dass die Verbindung getrennt wurde. Jedoch kann er mit senden einer neuen Nachrichten eine neue Verbindung herstellen. Der Client selber kann auch von sich aus die Verbindung trennen, indem er das Kommando `!EXIT` an den Server sendet. Falls sich der Client im Chat umbenennen will, kann er per `!CHATNAME NeuerName` seinen neuen Namen an den Server senden. Beim Schließen des Servers werden alle Clients über das Ende des Chattens informiert.

#### Logger [java.util.logging](https://docs.oracle.com/javase/7/docs/api/java/util/logging/package-summary.html)
Per Logging lassen sich Meldungen über den Zustand und Verlauf eines Programms auf der Konsole anzeigen oder in externen Dateien abspeichern. Das Loggen dient dazu da, um den Verlauf eines Programms besser zu verstehen bzw. mit protokollieren zu können.

Mit folgenden Befehl erstellt man einen Logger, wobei dieser immer einer Hierarchie zugeordnet wird.
```java
public static Logger clientLogger = Logger.getLogger("client");
```
Einem Logger kann man Level setzen, so dass nur Einträge aufgezeigt werden, die diesem Level entsprechen bzw. kritischer sind. Es gibt folgende Level, von gereiht von sehr kritisch bis nice-to-have Meldungen:  
* SEVERE
* WARNING
* INFO
* CONFIG
* FINE
* FINER
* FINEST  

Wenn man alle Meldungen ansehen möchte, dann stellt man den Logger folgendermaßen ein:
```java
clientLogger.setLevel(Level.FINEST)
```
Meldungen werden mit folgenden Befehl erstellt:
```java
clientLogger.log(Level.INFO, "Meldung")
```
Alternativ kann man gleich mit dem Level-Namen einen Eintrag erstellen:
```java
clientLogger.info("Meldung")
```

#### JavaFX [javafx](https://docs.oracle.com/javafx/2/get_started/jfxpub-get_started.htm)
Die beiden grafischen Oberflächen von Client und Server sind mit JavaFX realisiert. JavaFX ist ein Framework zur Gestaltung von grafischen Benutzeroberflächen in Java und geistiger Nachfolger von AWT und SWING. Entstanden ist er, weil die vorher erwähnten Standardlösungen nicht mehr wirklich zeitgerecht waren.

Eine Besonderheit ist das Szenen nicht nur per Programmcode sondern auch durch eine XML-Datei beschrieben werden können, die FXML gennant wird. Folgendes Bild beschreibt den Allgemeinen Aufbau von JavaFX klar dar:
![https://www.callicoder.com/javafx-fxml-form-gui-tutorial/](resources/javafx_fxml_application_structure.jpg)

JavaFX-Aufbau [1]

Eine Sache die bei der Implementierung aufgefallen ist diese, dass Veränderungen an der grafischen Oberfläche nur im JavaFX-Thread gemacht werden dürfen. Falls man aber von einem anderen Thread eine Änderung machen möchte, kann man einfach dem Thread sagen, was per `Runnable`sagen, was er tun soll. Dies wird durch folgenden Aufruf realisiert:
```java
Platform.runLater(Runnable runnable)`
```
Zum Beispiel sieht man Methode zum Hinzufügen von Text am Textfeld im Controller so aus:
```java
public void updateTextAreaWithText(String text) {
  Platform.runLater(() -> textArea.appendText("\n" + text));
}
```
Man kann im `FXML`-File angeben, dass das Anklicken von Komponenten eine Methode aufruft:
```
<TextField fx:id="textField"
                   onAction="#onEnter"
                   text=""
                   editable="true"
                   HBox.hgrow="ALWAYS"
                   maxWidth="Infinity"/>
<Button text="Send" onAction="#handleMessageButtonAction"/>
```
Diese Methoden werden dann entsprechend im Controller umgesetzt und mit `@FXML`-Annotation angeführt.
```java
/**
 * Reacts to button click event.
 * Calls {@link #sendMessage()}
 */
@FXML
protected void handleMessageButtonAction(ActionEvent event) {
    this.sendMessage();
}

/**
 * Reacts to pressed Enter-key event on the textfield.
 * Calls {@link #sendMessage()}
 */
@FXML
public void onEnter(ActionEvent event){
    this.sendMessage();
}
```
Um die Oberfläche wirklich resizable zu machen, kann man im `FXML`-File im Hauptlayout-Element Constraints einfügen um damit den Spalten und Zeilen per `percentWidth`zu sagen, dass sie immer eine prozentuelle Größe besitzen soll:
```xml
<columnConstraints>
    <ColumnConstraints hgrow="SOMETIMES" minWidth="10.0" percentWidth="80.0" prefWidth="100.0" />
    <ColumnConstraints hgrow="SOMETIMES" minWidth="20.0" percentWidth="20.0" prefWidth="100.0" />
</columnConstraints>
<rowConstraints>
    <RowConstraints minHeight="10.0" percentHeight="90" vgrow="ALWAYS" />
    <RowConstraints minHeight="20.0"  vgrow="SOMETIMES" />
</rowConstraints>
```

### Testung der grafischen Oberfläche
#### TestFX [API-Repo](https://github.com/TestFX/TestFX)
Ist ein Framework zum Testen von JavaFX. Es bietet viele Funktionen, besitzt aber leider eine eher weniger umfangreiche Dokumentation. Im Rep kann man eine [Wiki-Seite](https://github.com/TestFX/TestFX/wiki), die paar Funktionen zur Schau stellt. Examples findet man am besten bei den Issues.

Zur Verwendung unter Gradle sollte man die neusten Dependencies holen:
```
dependencies {
  testCompile "org.testfx:testfx-core:4.0.15-alpha"
  testCompile "org.testfx:testfx-junit:4.0.15-alpha"
}
```

##### Schreiben von Tests
Die Testklassen erben immer von der Klasse `ApplicationTest`. Man definiert immer eine Startklasse, die beim Starten die Instanz der grafischen Oberfläche bildet:
```java
@Override
public void start(Stage stage) throws Exception {
    mainNode = FXMLLoader.load(FXApplication.class.getResource("/client.fxml"));
    stage.setScene(new Scene(mainNode, 300, 275));
    stage.show();
    stage.toFront();
}
```
Jetzt kann man einfache Tests per Methode `verifyThat` machen, diese erhält ein Element oder Beschreibung als Querie und schaut ob dieses Element in der GUI wirklich existiert.
```java
@Test
// Schauen ob ein Button namens Send existiert
public void testHasButton() {
    verifyThat(".button", hasText("Send"));
}

@Test
// Schauen ob Grundlayout ein Textfeld besitzt
public void testHasTextArea() {
    verifyThat("#grid", hasChildren(1, "#textArea"));
}
```
Wirklich komplexere Abfragen und Test können mit dieser Methode nicht gemacht werden. Doch da man oben das `mainNode`-Element der GUI definiert hat, kann man aus diesem Ursprungselement das gewünschte Element raussuchen und die passenden Methoden an diesem erhaltenen Objekt aufrufen.
```java
@Test
public void testTextFieldText() {
    TextArea area = from(mainNode).lookup("#textArea").query();
    String text = area.getText();
    assertEquals("Welcome to Simple Chat!", area.getText());
}
```
Die API besitzt aber noch viele Funktionen mehr. Man kann beispielsweise Userverhalten durch sogenannten `FXRobots` nachempfinden lassen. Diese können den Mauszeiger manipulieren, Tastenanschläge machen und mehr. Die Mauszeigerbewegung ist nicht ganz fehlerlos, unter Windows besitzt mein Laptopbildschirm eine Skalierung von über 200%, dies überprüft die API nirgends und bewegt den Zeiger zum falschen Ort. Auf einem externen Bildschirm mit unter 200% Skalierung, funktionierte aber der Roboter ohne Probleme.
```java
@Test
public void testSendingMessage() {
    // Simulate a message sending
    // Robot clicks on textfield, writes "Baum" and clicks
    // the send button
    robot.clickOn("#textField");
    robot.write("Baum");
    robot.clickOn(".button");
    sleep(100);
    TextArea area = from(mainNode).lookup("#textArea").query();
    String text = area.getText();
    assertEquals("Welcome to Simple Chat!\nBaum", area.getText());
}
```
Weiters können auch Dialogmeldungen abgeprüft werden. Um diese Elemente zu finden, gibt man unter `targetWindow` ihren Fensternamen ein:
```java
@Test
public void testConnectionEndByUser() {
    // Simualte leaving chat
    robot.clickOn("#textField");
    robot.write("!EXIT");
    robot.clickOn(".button");
    // Get component from connection end dialog
    Button button = targetWindow("Connection ended").lookup(".button").queryButton();
    assertNotNull(button);
    robot.targetWindow("Connection ended").clickOn(".button");
}
```
Zur weiteren Testung habe ich mir Mock-Objekte mit Hilfe des Frameworks Mockito erstellt.
```java
client = mock(SimpleChat.class);
```
Mock-Objekten kann man sagen, was sie tun sollen bzw. zurückgeben sollen, wenn eine ihrer Methoden aufgerufen wird.
```java
// When .isConnected() method is called return the first
// time true, the next time false
when(client.isConnected()).thenReturn(true).thenReturn(false);
```
Man kann aber dem Mock-Objekt auch sagen, dass er seine normale Implementierung der Methode verwenden soll:
```java
Mockito.doCallRealMethod().when(client).setController(Mockito.any(Controller.class));
```
Außerdem kann man kann man ganze void-Methoden des Objektes neu für das mocking-Objekt definieren. In der folgenden Methode definiere ich, dass wenn der Controller der GUI eine Nachricht an den Server schicken will, er einfach sofort ohne Umwege die Nachricht anzeigt:
```java
Mockito.doAnswer(invocation -> {
        Object[] args = invocation.getArguments();
        String message = (String) args[0];
        client.incomingMessage(message);
        return null;
}).when(client).sendMessage(Mockito.anyString());
```
Ich hatte eine Zeit lang das Problem, dass wenn ich offene Fenster hatte, der erste Test per Roboter Interaktion nicht funktionierte, weil die Anwendung hinter dem offenen Fenster war und nicht ganz vorne angezeigt worden ist. Um die JavaFX-Anwendung nach vorne zu bringen und den Fehler zu beheben, habe ich den betroffenen Testklassen folgenden Befehl hinzugefügt
```java
stage.setAlwaysOnTop(true); 
```


## Quellen
\[1] Logger: <https://stackoverflow.com/questions/9199598/using-java-util-logging-to-log-on-the-console>

\[2] Thread: <https://stackoverflow.com/questions/10256891/how-to-start-a-background-thread-that-doesnt-block-the-main-thread-in-java>

\[3] Thread: <https://stackoverflow.com/questions/8579657/whats-the-difference-between-thread-start-and-runnable-run>

\[4] ListView: <https://stackoverflow.com/questions/36781355/javafx-listview-not-displaying-in-gui>

\[5] JavaFX-Aufbau: <https://www.callicoder.com/javafx-fxml-form-gui-tutorial/>

\[6] JavaFX-Thread: <https://stackoverflow.com/questions/21083945/how-to-avoid-not-on-fx-application-thread-currentthread-javafx-application-th>

\[7] JavaFX-Dialog: <https://code.makery.ch/blog/javafx-dialogs-official/>

\[8] JavaFX-Resizable: <https://stackoverflow.com/questions/36168429/javafx-gridpane-dynamic-resizng-of-child-nodes-to-fill-assigned-area>

\[9] JavaFX-Resizable: <https://docs.oracle.com/javafx/2/api/javafx/scene/layout/ConstraintsBase.html>

\[10] JavaFX-Enter-Listener: <https://stackoverflow.com/questions/13880638/how-do-i-pick-up-the-enter-key-being-pressed-in-javafx2>

\[11] Testen: <https://docs.google.com/document/d/1YPXJN6YCaRE13hGrHpO1eDsaKTEhUt6PkxbkhHQTuik/edit#>

\[12] TestFX: <https://github.com/TestFX/TestFX>

\[13] TestFX-Assertions: <https://github.com/TestFX/TestFX/wiki/Assertions>

\[14] TestFX-Assertions: <https://github.com/TestFX/TestFX/issues/57>

\[15] Mockito: <https://stackoverflow.com/questions/14970516/use-mockito-to-mock-some-methods-but-not-others>

## Abbildungsverzeichnis
\[1] <https://www.callicoder.com/javafx-fxml-form-gui-tutorial/>
