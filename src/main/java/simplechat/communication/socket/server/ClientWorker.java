package simplechat.communication.socket.server;

import simplechat.communication.MessageProtocol;
import simplechat.server.SimpleChat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

/**
 * Thread for client socket connection.<br>
 * Every client has to be handled by an own Thread.
 */
class ClientWorker implements Runnable {
    private Socket client;
    private String chatName = "Client#1";
    private PrintWriter out;
    private BufferedReader in;

    private SimpleChatServer callback;
    private boolean listening = true;

    /**
     * Init of ClientWorker-Thread for socket intercommunication
     *
     * @param client   Socket got from ServerSocket.accept()
     * @param callback {@link SimpleChatServer} reference
     * @throws IOException will be throwed if the init of Input- or OutputStream fails
     */
    ClientWorker(Socket client, SimpleChatServer callback) throws IOException {
        this.client = client;
        this.callback = callback;
    }

    /**
     * MessageHandler for incoming Messages on Client Socket
     * <br>
     * The InputSocket will be read synchronous through readLine()
     * Incoming messages first will be checked if they start with any Commands, which will be executed properly.
     * Otherwise text messages will be delegated to the {@link SimpleChatServer#received(String, ClientWorker)} method.
     */
    @Override
    public void run() {
        try {
            this.in = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
            this.out = new PrintWriter(this.client.getOutputStream(), true);
            String initName = this.in.readLine();
            if (!initName.equals("")) {
                chatName = callback.initName(initName.split(" ")[1], this);
            }
            callback.incomingName(chatName);
            Thread.sleep(1000);
            this.out.println("| Joined chat as " + chatName + " |");
        }
        catch (Exception ex) {
            SimpleChat.serverLogger.log(SEVERE, "Error in initializing Thread, Thread will be shutdown\n" +
                    "Error message: " + ex.getMessage());
        }
        String message = "";
        while (listening) {
            try {
                message = this.in.readLine();
                if(!message.substring(0, 1).equals("!")) {
                    callback.received(message, this);
                }
                else {
                    MessageProtocol.Commands command;
                    String[] fullCommand = message.split(" ");
                    command = MessageProtocol.getCommand(fullCommand[0]);
                    switch (command) {
                        case EXIT:
                            this.listening = false;
                            this.callback.removeClient(this);
                            break;
                        case CHATNAME:
                            callback.setName(fullCommand[1], this);
                            break;
                    }
                }
            }
            catch (Exception ex) {
                if(ex instanceof IOException) {
                    SimpleChat.serverLogger.log(INFO, "Error in Input/Output: " + ex.getMessage());
                }
                else if(ex instanceof IllegalArgumentException) {
                    SimpleChat.serverLogger.log(INFO, "Received Command [" + message + "] does not exist!");
                }
                else SimpleChat.serverLogger.log(SEVERE, ex.getMessage());
            }
        }
    }

    /**
     * Clean shutdown of ClientWorker
     * <br>
     * If listening was still true, we are sending a {@link MessageProtocol.Commands#EXIT} to the client.
     * Finally we are closing all open resources.
     */
    void shutdown() {
        SimpleChat.serverLogger.log(INFO, "Shutting down ClientWorker ... listening=" + listening);
        if (listening) {
            this.out.println("!EXIT");
        }
        listening = false;
        try {
            client.close();
        }
        catch (IOException ex) {
            SimpleChat.serverLogger.log(SEVERE, "Error in closing ClientWorker-Socket: " + ex.getMessage());
        }
    }

    /**
     * Sending message through Socket OutputStream {@link #out}
     *
     * @param message MessageText for Client
     */
    void send(String message) {
        this.out.println(message);
    }

    /**
     * Sets a new name for the client
     *
     * @param chatName New client name
     */
    public void setChatName(String chatName) {
        this.chatName = chatName;
    }
}
