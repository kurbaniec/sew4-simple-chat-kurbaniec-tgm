package simplechat.communication.socket.server;

import javafx.concurrent.Worker;
import simplechat.communication.MessageProtocol;
import simplechat.server.SimpleChat;

import static java.util.logging.Level.*;
import static simplechat.communication.MessageProtocol.Commands.EXIT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


/**
 * SimpleChatServer listens to incoming SimpleChatClients with the choosen communication protocol and initiates a UI.
 * <br>
 * Default settings for the main attributes will be: host="localhost" port=5050 and backlog=5
 */
public class SimpleChatServer extends Thread {

    private Integer port = 5050;
    private String host = "localhost";
    private final Integer backlog = 5;
    private ServerSocket serverSocket = null;

    private boolean listening = false;
    private SimpleChat server;

    private ConcurrentHashMap<ClientWorker, String> workerList = new ConcurrentHashMap<>();
    private ExecutorService executorService = Executors.newCachedThreadPool();


    /**
     * Initializes host, port and callback for UserInterface interactions.
     *
     * @param host   String representation of hostname, on which the server should listen
     * @param port   Integer for the listening port
     * @param server UserInterface callback reference for user interactions
     */
    public SimpleChatServer(String host, Integer port, SimpleChat server) {
        if (host != null) this.host = host;
        if (port != null) this.port = port;
        this.server = server;
        SimpleChat.serverLogger.log(INFO, "Init: host=" + this.host + " port=" + this.port);

    }

    /**
     * Initiating the ServerSocket with already defined Parameters and starts accepting incoming
     * requests. If client connects to the ServerSocket a new ClientWorker will be created and passed
     * to the ExecutorService for immediate concurrent action.
     */
    public void run() {
        SimpleChat.serverLogger.log(INFO, "... starting Thread ...");
        try {
            serverSocket = new ServerSocket(port);
        }
        catch (IOException ex) {
            SimpleChat.serverLogger.info("Got an exception: " + ex.getMessage());
        }
        listening = true;
        // Save client number
        String chatName;
        while(listening) {
            try {
                ClientWorker worker = new ClientWorker(serverSocket.accept(), this);
                chatName = server.addClient("");
                worker.setChatName(chatName);
                workerList.put(worker, chatName);
                SimpleChat.serverLogger.log(INFO, "New Worker: " + chatName);
                executorService.execute(worker);
            }
            catch (Exception ex) {
                if (ex instanceof IOException) {
                    SimpleChat.serverLogger.info("Got an exception: " + ex.getMessage());
                }
                else if (ex instanceof NullPointerException) {
                    SimpleChat.serverLogger.info("Address already in use: NET_Bind -> Shutting down Server");
                    listening = false;
                    server.stop();
                }
                else SimpleChat.serverLogger.info(ex.getMessage());
            }
        }
    }

    /**
     * Callback method for client worker to inform server of new message arrival.
     * Also sends the received message with client name per {@link #send(String)}
     * to all connected clients.
     *
     * @param plainMessage MessageText sent to server without Client information
     * @param sender       {@link ClientWorker} which received the message
     */
    public void received(String plainMessage, ClientWorker sender) {
        String client = "["+ workerList.get(sender) + "]";
        SimpleChat.serverLogger.info("Got message from Client" + client + ": " + plainMessage);
        server.incomingMessage(client + ": " + plainMessage);
        this.send(client + ": " + plainMessage);

    }

    /**
     * Sending messages to clients through communication framework.
     *
     * @param message MessageText with sender ChatName
     */
    public void send(String message) {
        SimpleChat.serverLogger.info("Sending message to all clients: " + message);
        for(ClientWorker worker : workerList.keySet()) {
            worker.send(message);
        }
    }

    /**
     * Sending message to one client through communication framework
     *
     * @param message  MessageText with sender ChatName
     * @param receiver ChatName of receiving Client
     */
    public void send(String message, Object receiver) {
        ClientWorker rec = (ClientWorker)receiver;
        rec.send(message);
    }

    /**
     * ClientWorker always sends his ChatName at the beginning. This method checks if it´s
     * a valid name. Also stores the returned Name in the ClientWorker-Collection.
     *
     * @param chatName name that client wants to use in the chat
     * @param worker   ClientWorker Thread which was initiating the renaming
     */
    String initName(String chatName, ClientWorker worker) {
        String oldChatName = workerList.get(worker);
        String newChatName = server.renameClientSilent(workerList.get(worker), chatName);
        workerList.put(worker, newChatName);
        return newChatName;
    }

    /**
     * Method asks the UI to add a new Message, that a client joined the server
     * @param chatName Client chatname that joined the server
     */
    void incomingName(String chatName) {
        server.incomingMessage("| Client joined chat as " + chatName + " |");
    }

    /**
     * ClientWorker has the possibility to change the ChatName. This method asks the UI
     * to rename the Client and store the returned Name in the ClientWorker-Collection.
     * Also send via {@link #send(String)} information about the rename-Operation to all
     * clients
     *
     * @param chatName new Name of Client
     * @param worker   ClientWorker Thread which was initiating the renaming
     */
    void setName(String chatName, ClientWorker worker) {
        String oldChatName = workerList.get(worker);
        String newChatName = server.renameClient(workerList.get(worker), chatName);
        workerList.put(worker, newChatName);
        this.send("| RENAME: " + oldChatName + " -> " + newChatName + " |");
        this.send("!CHATNAME " + newChatName, worker);
    }


    /**
     * Remove only this worker from the list,
     * shutdown the ClientWorker and also inform GUI about removal.
     *
     * @param worker ClientWorker which should be removed
     */
    void removeClient(ClientWorker worker) {
        worker.shutdown();
        server.removeClient(workerList.get(worker));
        workerList.remove(worker);
    }

    /**
     * Gets the ClientWorker of the given chatName and calls the private Method {@link #removeClient(String)}
     * This method will remove the worker from the list shutdown the ClientWorker and also inform GUI about removal.
     *
     * @param chatName Client name which should be removed
     */
    public void removeClient(String chatName) {
        for (ClientWorker worker : workerList.keySet()) {
            if (workerList.get(worker).equals(chatName)) {
                worker.shutdown();
                workerList.remove(worker);
                break;
            }
        }
    }

    /**
     * Clean shutdown of all connected Clients.<br>
     * ExecutorService will stop accepting new Thread inits.
     * After notifying all clients, ServerSocket will be closed and ExecutorService will try to shutdown all
     * active ClientWorker Threads.
     */
    public void shutdown() {
        listening = false;
        SimpleChat.serverLogger.log(INFO, "Shutting down SimpleChatServer ... Stopping executor ... [25%]");
        executorService.shutdown();
        SimpleChat.serverLogger.log(INFO, "Shutting down SimpleChatServer ... Notifying clients ... [50%]");
        for(ClientWorker worker : workerList.keySet()) {
            worker.shutdown();
        }
        try {
            SimpleChat.serverLogger.log(INFO, "Shutting down SimpleChatServer ... Closing socket ... [75%]");
            serverSocket.close();
            SimpleChat.serverLogger.log(INFO, "Shutting down SimpleChatServer ... Terminate running threads ... [99%]");
            executorService.shutdownNow();
            executorService.awaitTermination(1000, TimeUnit.MILLISECONDS);

        }
        catch (Exception ex) {
            if (ex instanceof IOException) {
                SimpleChat.serverLogger.log(SEVERE, "Error in closing ServerSocket: " + ex.getMessage());
            }
            else if (ex instanceof InterruptedException) {
                SimpleChat.serverLogger.log(SEVERE, "Problem with shutdown of threads in executor service: " + ex.getMessage());
            }
            else SimpleChat.serverLogger.log(SEVERE, ex.getMessage());
        }
    }
}

