package simplechat.client;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import simplechat.communication.MessageProtocol;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.logging.Level.SEVERE;
import static simplechat.communication.MessageProtocol.Commands.EXIT;

public class Controller {

    private SimpleChat simpleChat;

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    @FXML
    private TextField textField;

    @FXML
    private TextArea textArea;

    @FXML
    private Text actionTarget = null;

    /**
     * Reacts to button click event.
     * Calls {@link #sendMessage()}
     */
    @FXML
    protected void handleMessageButtonAction(ActionEvent event) {
        this.sendMessage();
    }

    /**
     * Reacts to pressed Enter-key event on the textfield.
     * Calls {@link #sendMessage()}
     */
    @FXML
    public void onEnter(ActionEvent event){
        this.sendMessage();
    }

    public void initialize() {
    }

    public void stop() {
        if (simpleChat.isConnected()) {
            simpleChat.stop(false);
        }
        Platform.exit();
    }

    public void setSimpleChat(SimpleChat simpleChat) {
        this.simpleChat = simpleChat;
    }

    public void updateTextAreaWithText(String text) {
        Platform.runLater(() -> textArea.appendText("\n" + text));
    }

    /**
     * Evaluates input of textField.
     * <br>
     * If User enters proper {@link simplechat.communication.MessageProtocol.Commands} this method will act accordingly.
     * <br>
     * {@link simplechat.communication.MessageProtocol.Commands#EXIT} will shutdown the whole client with the
     * call to {@link simplechat.client.SimpleChat#stop()}
     * <br>
     * If there is now Command (no "!" as first character),
     * the message will be passed to {@link simplechat.client.SimpleChat#sendMessage(String)}
     */
    public void sendMessage() {
        String message = textField.getText();
        if(!message.equals("")) {
            if (!message.substring(0, 1).equals("!")) {
                if (!simpleChat.isConnected()) {
                    simpleChat.restart();
                    try {
                        Thread.sleep(1000);
                    }
                    catch (Exception ex) {
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Something went wrong!");
                        alert.setHeaderText("Something went wrong!");
                        alert.showAndWait();
                    }
                }
                simpleChat.sendMessage(message);

            }
            else {
                String[] fullCommand = message.split(" ");
                try {
                    MessageProtocol.Commands command = MessageProtocol.getCommand(fullCommand[0]);
                    switch (command) {
                        case EXIT:
                            simpleChat.stop();
                            break;
                        case CHATNAME:
                            simpleChat.sendMessage(message);
                            break;
                    }
                }
                catch (IllegalArgumentException iae) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Unknown Command");
                    alert.setHeaderText("The entered command does not exist!");
                    alert.setContentText("Valid commands:\n" +
                            "!EXIT - Close program\n!CHATNAME [NEW_NAME] - Set a new name");
                    alert.showAndWait();
                }
            }
            Platform.runLater(() -> textField.clear());
        }
    }

    /**
     * Shows the client an dialog, to inform him, that the
     * connection to the server was closed.
     */
    public void showConnectionEnd() {
        Platform.runLater(() -> {
            textField.clear();
            textArea.clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Connection ended");
            alert.setHeaderText("Connection to chat has ended");
            alert.setContentText("Send a new message to reenter the chat.");
            alert.showAndWait();
        });

    }

    Runnable clearText = () -> {
        actionTarget.setText("");
    };
}
