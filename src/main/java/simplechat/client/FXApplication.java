package simplechat.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import static javafx.scene.control.Alert.AlertType.*;

public class FXApplication extends Application {

    private Controller controller;
    private static SimpleChat simpleChat;
    private Parent root;

    public FXApplication() { }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/client.fxml"));
        root = loader.load();
        this.controller = loader.getController();

        controller.setSimpleChat(simpleChat);
        simpleChat.setController(controller);

        Scene scene = new Scene(root, 300, 270);

        stage.setTitle("Simple Chat - Client");
        stage.setMinWidth(250);
        stage.setMinHeight(150);

        stage.setScene(scene);
        stage.show();
        stage.toFront();

        if(!simpleChat.isConnected()) {
            Alert alert = new Alert(ERROR);

            alert.setTitle("Communication error");
            alert.setHeaderText("Server not reachable!");
            alert.setContentText("Please check your parameters and try it again.");
            alert.showAndWait();
            Platform.exit();
        }
    }

    public void setSimpleChat(SimpleChat simpleChat) {
        this.simpleChat = simpleChat;
    }

    @Override
    public void stop(){
        controller.stop();
    }

    public Parent getRoot() {
        return root;
    }
}
