package simplechat.server;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller {

    private SimpleChat simpleChat;

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    @FXML
    private TextField textField;

    @FXML
    private TextArea textArea;

    @FXML
    private ListView listView;

    @FXML
    private Text actionTarget = null;

    @FXML
    protected void handleRemoveButtonAction(ActionEvent event) {
        String user = (String)listView.getSelectionModel().getSelectedItem();
        if (user != null) {
            //removeUser(user);
            simpleChat.shutdownClient(user);
        }
    }


    public void initialize() {

    }

    public void stop() {
        simpleChat.stop();
        Platform.exit();
    }

    public void setSimpleChat(SimpleChat simpleChat) {
        this.simpleChat = simpleChat;
    }

    public void updateTextAreaWithText(String text) {
        Platform.runLater(() -> textArea.appendText("\n" + text));
    }

    public void addUser(String user) {
        Platform.runLater(() -> listView.getItems().add(user));
    }

    public void removeUser(String user) {
        //simpleChat.removeClient(user);
        Platform.runLater(() -> listView.getItems().remove(user));
    }

    /**
     * Reacts to button click event.
     * Calls {@link #sendMessage()}
     */
    @FXML
    protected void handleMessageButtonAction(ActionEvent event) {
        this.sendMessage();
    }

    /**
     * Reacts to pressed Enter-key event on the textfield.
     * Calls {@link #sendMessage()}
     */
    @FXML
    public void onEnter(ActionEvent event){
        this.sendMessage();
    }

    /**
     * Informs Server, that admin wants to send a message to all clients
     * the message will be passed to {@link simplechat.server.SimpleChat#sendMessage(String)}
     */
    public void sendMessage() {
        String message = textField.getText();
        if(!message.equals("")) {
            simpleChat.sendMessage(message);
            Platform.runLater(() -> textField.clear());
        }
    }

    Runnable clearText = () -> {
        actionTarget.setText("");
    };
}
