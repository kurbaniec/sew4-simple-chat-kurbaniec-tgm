package simplechat.gui;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.util.WaitForAsyncUtils;
import simplechat.client.Controller;
import simplechat.client.FXApplication;
import simplechat.client.SimpleChat;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.hasChildren;
import static org.testfx.matcher.control.LabeledMatchers.hasText;

/**
 * Checks if the graphical interface and controller are working correctly with
 * the help of an mock object of SimpleChat
 * @author Kacper Urbaniec
 * @version 23.01.2019
 */
public class ClientMockTest extends ApplicationTest {
    private Parent mainNode;
    private SimpleChat client;
    private FxRobot robot;

    @Override
    public void start(Stage stage) throws Exception {
        // Create mock
        client = mock(SimpleChat.class);
        // Define methods
        when(client.isConnected()).thenReturn(true).thenReturn(false);
        Mockito.doCallRealMethod().when(client).setController(Mockito.any(Controller.class));
        Mockito.doCallRealMethod().when(client).incomingMessage(Mockito.anyString());
        Mockito.doCallRealMethod().when(client).getController();
        Mockito.doAnswer(invocation -> {
                Object[] args = invocation.getArguments();
                String message = (String) args[0];
                client.incomingMessage(message);
                return null;
        }).when(client).sendMessage(Mockito.anyString());
        Mockito.doAnswer(invocation -> {
            client.getController().showConnectionEnd();
            return null;
        }).when(client).stop();
        // Define robot
        robot = new FxRobot();
        // Start interface and get root window
        FXApplication fxApplication = new FXApplication();
        fxApplication.setSimpleChat(client);
        stage.setAlwaysOnTop(true);
        fxApplication.start(stage);
        mainNode = fxApplication.getRoot();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void testIncomingMessage() {
        client.incomingMessage("Hallo");
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!\nHallo", area.getText());
    }

    @Test
    public void testIncomingMessageSpam() {
        Thread[] runners = new Thread[10];
        for(int i = 0; i < runners.length; i++) {
            final int j = i;
            runners[i] = new Thread(() -> {
                client.incomingMessage("Msg" + j);
            });
            runners[i].start();
        }
        for(Thread runner: runners) {
            try {
                runner.join();
            }
            catch (Exception ex) {
                fail();
            }
        }
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        for(int i = 0; i < runners.length; i++) {
            if(!text.contains("Msg" + i)) fail();
        }
    }

    @Test
    public void testSendingMessage() {
        // Simulate a message sending
        robot.clickOn("#textField");
        robot.write("Baum");
        robot.clickOn(".button");
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!\nBaum", area.getText());
    }

    @Test
    public void testSendingMessage2() {
        // Simulate a message sending
        robot.clickOn("#textField");
        robot.write("Baum");
        robot.press(KeyCode.ENTER);
        robot.release(KeyCode.ENTER);
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        assertEquals("Welcome to Simple Chat!\nBaum", area.getText());
    }

    @Test
    public void testSendingMessageSpam() {
        // Simulate a message sending
        for(int i = 0; i < 3; i++) {
            robot.clickOn("#textField");
            robot.write("Spam");
            robot.press(KeyCode.ENTER);
            robot.release(KeyCode.ENTER);
        }
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!\nSpam\nSpam\nSpam", area.getText());
    }

    @Test
    public void testFalseCommand() {
        robot.clickOn("#textField");
        robot.write("!");
        robot.clickOn(".button");
        Button button = targetWindow("Unknown Command").lookup(".button").queryButton();
        // button.getText() sometimes print the wrong answer, so notNull is used
        // fullfills the same idea
        assertNotNull(button);
        robot.targetWindow("Unknown Command").clickOn(".button");
    }

    @Test(expected = NoSuchElementException.class)
    public void testFalseCommand2() {
        robot.clickOn("#textField");
        robot.write("!");
        robot.clickOn(".button");
        robot.targetWindow("Unknown Command").clickOn(".button");
        Button button = targetWindow("Unknowns Command").lookup(".button").queryButton();
    }

    @Test
    public void testConnectionEndByUser() {
        robot.clickOn("#textField");
        robot.write("!EXIT");
        robot.clickOn(".button");
        Button button = targetWindow("Connection ended").lookup(".button").queryButton();
        assertNotNull(button);
        robot.targetWindow("Connection ended").clickOn(".button");
    }

}
