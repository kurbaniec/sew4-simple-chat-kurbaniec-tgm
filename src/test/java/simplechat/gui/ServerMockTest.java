package simplechat.gui;

import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import simplechat.server.Controller;
import simplechat.server.FXApplication;
import simplechat.server.SimpleChat;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Checks if the graphical interface and controller are working correctly with
 * the help of an mock object of SimpleChat
 * @author Kacper Urbaniec
 * @version 24.01.2019
 */
public class ServerMockTest extends ApplicationTest {
    private Parent mainNode;
    private SimpleChat server;
    private FxRobot robot;

    @Override
    public void start(Stage stage) throws Exception {
        // Create mock
        server = mock(SimpleChat.class);
        // Define methods
        Mockito.doCallRealMethod().when(server).setController(Mockito.any(Controller.class));
        Mockito.doCallRealMethod().when(server).incomingMessage(Mockito.anyString());
        Mockito.doCallRealMethod().when(server).getController();
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String message = (String) args[0];
            server.getController().updateTextAreaWithText(message);
            return null;
        }).when(server).incomingMessage(Mockito.anyString());
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String user = (String) args[0];
            server.getController().addUser(user);
            return null;
        }).when(server).addClient(Mockito.anyString());
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String user = (String) args[0];
            server.getController().removeUser(user);
            return null;
        }).when(server).removeClient(Mockito.anyString());
        Mockito.doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            String message = (String) args[0];
            server.incomingMessage(message);
            return null;
        }).when(server).sendMessage(Mockito.anyString());
        // Define robot
        robot = new FxRobot();
        // Start interface and get root window
        FXApplication fxApplication = new FXApplication();
        fxApplication.setSimpleChat(server);
        stage.setAlwaysOnTop(true);
        fxApplication.start(stage);
        mainNode = fxApplication.getRoot();
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void testIncomingMessage() {
        server.incomingMessage("Hallo");
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!\nHallo", area.getText());
    }

    @Test
    public void testIncomingMessageSpam() {
        Thread[] runners = new Thread[10];
        for(int i = 0; i < runners.length; i++) {
            final int j = i;
            runners[i] = new Thread(() -> {
                server.incomingMessage("Msg" + j);
            });
            runners[i].start();
        }
        for(Thread runner: runners) {
            try {
                runner.join();
            }
            catch (Exception ex) {
                fail();
            }
        }
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        for(int i = 0; i < runners.length; i++) {
            if(!text.contains("Msg" + i)) fail();
        }
    }

    @Test
    public void testAddClient() {
        server.addClient("Franz");
        ListView listView = from(mainNode).lookup("#listView").query();
        ObservableList list = listView.getItems();
        assertEquals("Franz", list.get(0));
    }

    @Test
    public void testAddClientSpam() {
        Thread[] runners = new Thread[10];
        for(int i = 0; i < runners.length; i++) {
            final int j = i;
            runners[i] = new Thread(() -> {
                server.addClient("Franz#" + j);
            });
            runners[i].start();
        }
        for(Thread runner: runners) {
            try {
                runner.join();
            }
            catch (Exception ex) {
                fail();
            }

        }
        sleep(100);
        ListView listView = from(mainNode).lookup("#listView").query();
        ObservableList list = listView.getItems();
        if (list.size() != runners.length) fail();
    }

    @Test
    public void testRemoveClient() {
        server.addClient("Franz");
        server.removeClient("Franz");
        sleep(100);
        ListView listView = from(mainNode).lookup("#listView").query();
        ObservableList list = listView.getItems();
        if (list.size() > 0) fail();
    }

    @Test
    public void testRemoveClientSpam() {
        Thread[] adders = new Thread[10];
        for(int i = 0; i < adders.length; i++) {
            final int j = i;
            adders[i] = new Thread(() -> {
                server.addClient("Franz#" + j);
            });
            adders[i].start();
        }
        for(Thread runner: adders) {
            try {
                runner.join();
            }
            catch (Exception ex) {
                fail();
            }
        }
        Thread[] removers = new Thread[10];
        for(int i = 0; i < removers.length; i++) {
            final int j = i;
            removers[i] = new Thread(() -> {
                server.removeClient("Franz#" + j);

            });
            removers[i].start();
        }
        for(Thread runner: removers) {
            try {
                runner.join();
            }
            catch (Exception ex) {
                fail();
            }
        }
        sleep(100);
        ListView listView = from(mainNode).lookup("#listView").query();
        ObservableList list = listView.getItems();
        if (list.size() != 0) fail();
    }

    @Test
    public void testSendingMessage2() {
        // Simulate a message sending
        robot.clickOn("#textField");
        robot.write("Baum");
        robot.press(KeyCode.ENTER);
        robot.release(KeyCode.ENTER);
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        assertEquals("Welcome to Simple Chat!\nBaum", area.getText());
    }

    @Test
    public void testSendingMessage() {
        // Simulate a message sending
        robot.clickOn("#textField");
        robot.write("Baum");
        robot.clickOn(".button");
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!\nBaum", area.getText());
    }




    @Test
    public void testSendingMessageSpam() {
        // Simulate a message sending
        for(int i = 0; i < 3; i++) {
            robot.clickOn("#textField");
            robot.write("Spam");
            robot.press(KeyCode.ENTER);
            robot.release(KeyCode.ENTER);
        }
        sleep(100);
        TextArea area = from(mainNode).lookup("#textArea").query();
        String text = area.getText();
        assertEquals("Welcome to Simple Chat!\nSpam\nSpam\nSpam", area.getText());
    }
}
