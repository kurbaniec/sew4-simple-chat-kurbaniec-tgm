package simplechat.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import org.testfx.framework.junit.ApplicationTest;

/**
 * @author Kacper Urbaniec
 * @version 20.01.2019
 */
public class FXApplicationMockTest {}
/**
public class FXApplicationMockTest extends ApplicationTest {
    private Parent root;
    private ObservableList<String> text = FXCollections.observableArrayList();
    private MockClient mockClient;
    private static SimpleChat simpleChat;
    private static int iteration = 0;
    private static ArrayList<MockClient> arrayMockClient;

    @Override
    public void start(Stage stage) throws Exception {

        for (; iteration == 0; ++iteration) {
            simpleChat = new SimpleChat("localhost", 8888);
            simpleChat.listen();
            arrayMockClient = new ArrayList<MockClient>();
        }

        FXApplication fxApplication = new FXApplication();
        fxApplication.setSimpleChat(simpleChat);

        fxApplication.start(stage);

        root = fxApplication.root;

        sleep(1000);
        mockClient = new MockClient();
        arrayMockClient.add(mockClient);
    }


    @Before
    public void setUp() throws Exception {
    }


    @After
    public void tearDown() throws Exception {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }


    @Test
    public void testIsClientConnected() {
        // is the latest client connected?
        assert (mockClient.cS.isConnected());
    }

    @Test
    public void testClientSentMessage() {
        String msg = "This is a test!";
        mockClient.send(msg);
        TextArea textArea = (TextArea) GuiTest.find("#textArea");
        sleep(1000);
        String text = textArea.getText();
        assert (text.endsWith(msg));

    }
}*/